  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/methodology.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><div>Methodology</div></li>
					</ul>
				</div>
			</div>
			<div class="video_section">
				<div class="video_block">
					<iframe width="1280" height="533" src="https://www.youtube.com/embed/UhVjp48U2Oc?enablejsapi=1&version=3&playerapiid=ytplayer&mute=1&loop=1&autoplay=1&rel=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
				</div>
			</div>
			<div class="text_section">
				<div class="page_container">
					<h1 class="page_title">Methodology</h1>
					<div class="description_block">n this section we invite you to study what citizenship and civic education is, why it is possible, who is an active citizen and what are its types of activities. Understanding and "digesting" these topics is crucial if you are already involved in civic education or planning to teach.
						<br/>
						<br/>In this section we invite you to study what citizenship and civic education is, why it is possible, who is an active citizen and what are its types of activities. Understanding and "digesting" these topics is crucial if you are already involved in civic education or planning to teach. 
						<br/>
						<br/>At first glance, it may seem that everything is so simple and you know everything, but as you go deeper into their essence, as many discoveries as possible can be made on so many familiar topics that can form a more confident and solid foundation. Take the quality of your teaching to a new level. After all, a superficial educator can leave an irreversible mark on a citizen-building process.
						<br/>In this section we invite you to study what citizenship and civic education is, why it is possible, who is an active citizen and what are its types of activities.
						<br/><br/>
						Understanding and "digesting" these topics is crucial if you are already involved in civic education or planning to teach. At first glance, it may seem that everything is so simple and you know everything, but as you go deeper into their essence, as many discoveries as possible can be made on so familiar topics that can form a more confident and solid foundation. ։ Take the quality of your teaching to a new level. After all, a superficial educator can leave an irreversible mark on a citizen's formation process.
						<br/><br/>
						Take the quality of your teaching to a new level. After all, a superficial educator can leave an irreversible mark on a citizen's formation process.
						<br/><br/>
						Understanding and "digesting" these topics is crucial if you are already involved in civic education or planning to teach. At first glance, it may seem that everything is so simple and you know everything, but as you go deeper into their essence, as many discoveries as possible can be made on so many familiar topics that can form a more confident and solid foundation. ։ Take the quality of your teaching to a new level. After all, a superficial educator can leave an irreversible mark on a citizen-building process.
						<br/>In this section we invite you to study what citizenship and civic education is, why it is possible, who is an active citizen and what are its types of activities.
						<br/><br/>
						Understanding and "digesting" these topics is crucial if you are already involved in civic education or planning to teach. At first glance, it may seem that everything is so simple and you know everything, but as you go deeper into their essence, as many discoveries as possible can be made on so familiar topics that can form a more confident and solid foundation. ։ Take the quality of your teaching to a new level. After all, a superficial educator can leave an irreversible mark on a citizen's formation process.
						</div>
						<ul class="faq_list">
							<li>
								<div class="question_block">Ինչպե՞ս օգտագործել կայքը որպես արտիստ</div>
								<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							</li>
							<li>
								<div class="question_block">Ինչպե՞ս աշխատել կոմիքների և անիմացիաների հետ որպես կրթական նյութ</div>
								<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							</li>
							<li>
								<div class="question_block">Ինչպե՞ս կազմակերպել աշխատարաններ</div>
								<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							</li>
						</ul>
				</div>
			</div>
			
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>