  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/profile.css">
		
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/jquery.form-validator.js"></script>
		<script src="js/select.js"></script>
		<script src="js/attach.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="profile.php">My Profile</a></li>
						<li><div>Profile Edit</div></li>
					</ul>
				</div>
			</div>
			 <div class="profile_inner">
				<div class="page_container">
					<h1 class="page_title">Edit Profile</h1>
					<form class="edit_section">
						<div class="profile_image">
							<div class="fields_label">Profile image</div>
							<div class="attach_block">
								<div class="image_block"></div>
								<label class="attach_label">
									<input type="file" name="comics_poster" data-preview="on" data-maxsize="5" accept=".png, .jpg, .jpg" data-sizeerror="large file(max. 5mb)" data-validation="required" data-typeerror="invalid file format (.png, .jpg or .jpg)"/>
									<span class="attach_btn icon_edit">Edit Photo</span>
								</label>
							</div>
						</div>
						<div class="fields_section">
							<div class="fields_label">Personal</div>
							<div class="form_fields">
								<div class="field_block">
									<div class="field_name">Name</div>
									<input type="text" name="full_name" placeholder=" Name" data-validation="required"/>									<span class="error_hint">This section is requited </span>
								</div>
								<div class="field_block">
									<div class="field_name">Surname</div>
									<input type="text" name="full_name" placeholder="Surname" data-validation="required"/>
									<span class="error_hint">This section is requited </span>
								</div>
								<div class="field_block">
									<div class="field_name">Nickname</div>
									<input type="text" name="username" placeholder="Nickname" data-validation="required"/>
									<span class="error_hint">This section is requited </span>
								</div>
								<div class="field_block">
									<div class="field_name">Phone Number</div>
									<input type="text" name="phone_number" placeholder="Phone Number" data-validation="required" oninput="this.value=this.value.replace(/[^0-9+]/g,'');"/>
									<span class="error_hint">This section is requited</span>
								</div>
								<div class="field_block full_field">
									<div class="field_name">E-mail address</div>
									<div class="static_field">example@example.com</div>
								</div>
							</div>
							<div class="fields_label">About</div>
							<div class="form_fields">
								<div class="field_block">
									<div class="field_name">Country</div>
									<select name="country" data-placeholder="Country">
										<option></option>
										<option value="1">Armenia</option>
										<option value="2">Georgia</option>
										<option value="3">Russia</option>
										<option value="4">Belarus</option>
										<option value="5">Kazakhstan</option>
									</select>
								</div>
								<div class="field_block">
									<div class="field_name">City</div>
									<select name="country" data-placeholder="City" disabled>
										<option></option>
										<option value="1">Yerevan</option>
										<option value="2">Abovyan</option>
										<option value="3">Goris</option>
										<option value="4">Kapan</option>
										<option value="5">Meghri</option>
									</select>
								</div>
								<div class="field_block full_field">
									<div class="field_name">Bio</div>
									<textarea name="bio" placeholder="Bio"></textarea>
									<span class="error_hint">This section is requited</span>
								</div>
								<div class="field_block full_field">
									<div class="field_name">Bio</div>
									<div class="input_block">
										<input type="text" name="social_link[]" placeholder="Social media link"/>
									</div>
									<span class="add_field icon_add">Add new link</span>
								</div>
							</div>
							<div class="fields_label">Change Pasword</div>
							<div class="form_fields">
								<div class="field_block full_field">
									<div class="field_name">Old Password</div>
									<input type="password" data-field="old_pass" name="old_pass"  placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"/>
									<span class="error_hint">This section is requited</span>
									<span class="type_switch"></span>
								</div>
								<div class="field_block">
									<div class="field_name">New Password</div>
									<input class="password_field" data-field="new_pass" type="password" name="reg_pass" placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"/>
									<span class="error_hint">This section is requited</span>
									<span class="type_switch"></span>
								</div>
								<div class="field_block">
									<div class="field_name">Confirm new password</div>
									<input data-field="new_pass_confirm" type="password" name="pass_confirm" placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"/>
									<span class="error_hint">Passwords do not match</span>
									<span class="type_switch"></span>
								</div>
							</div>
							<div class="btn_block">
								<button type="submit"  class="validate_btn" data-popup="save_popup">Save changes</button>
							</div>
						</div>
						<div class="popup_block save_popup">
							<div class="popup_inner">
								<div class="popup_container">
									<div class="description_block">Are you sure you want to save changes? </div>
									<div class="btns_block">
										<span class="cancel_btn">Cancel</span>
										<button class="save_btn">Save</button>
									</div>
								</div>
							</div>
						</div>
					</form>
				</div>
			 </div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
		
	 	<script src="js/main.js"></script>
 	</body>
</html>