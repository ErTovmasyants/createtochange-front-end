  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/listing.css">
		<link rel="stylesheet" href="css/profile.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><div>My Profile</div></li>
					</ul>
				</div>
			</div>
			<div class="profile_inner">
				<div class="page_container">
					<div class="profile_main">
						<div class="image_socials">
							<div class="image_block">
								<div class="image_inner">
									<img src="images/profile.jpg" alt="" title=""/>	
								</div>
								<div class="edit_btn">
									<a href="profile_edit.php" class="icon_edit">Edit Profile</a>
								</div>
							</div>
							<ul class="socials_list">
								<li><a href="" target="_blank" class="icon_twitter_round"></a></li>
								<li><a href="" target="_blank" class="icon_facebook_round"></a></li>
								<li><a href="" target="_blank" class="icon_linkedin_round"></a></li>
								<li><a href="" target="_blank" class="icon_instagram_round"></a></li>
								<li><a href="" target="_blank" class="icon_youtube_round"></a></li>
								<li><a href="" target="_blank" class="icon_behance_round"></a></li>
							</ul>
						</div>
						<div class="info_block">
							<div class="logout_btn">
								<a href="" class="icon_exit">Log Out</a>
							</div>
							<h1 class="name_block">
								<span class="nickname">Angeloftheocean</span>
								<span class="fullname">(Diana Hovhannisyan)</span>
							</h1>
							<div class="description_block">
								<div class="location_block">Yerevan, Armenia</div>
								<div class="short_info">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
							</div>
							<div class="page_btns">
								<a href="profile.php" class="selected">Իմ կոմիքսները</a>
								<a href="draft.php">Սևագրություն (4)</a>
								<a href="" class="add_btn icon_plus">Ավելացնել կոմիքս</a>
							</div>
						</div>
					</div>
					<ul class="switch_buttons">
						<li><a href="" class="selected">Reading Comics</a></li>
						<li><a href="">Animated Comics</a></li>
					</ul>
					<ul class="products_list">
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image1.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image2.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image3.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image4.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image5.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image6.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image5.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image6.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image2.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image4.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image3.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image1.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
								
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
							<div class="action_btns">
								<button class="popup_btn icon_delete delete_btn" data-popup="delete_popup"></button>
							</div>
						</li>
					</ul>
					<div class="paging">
						<ul>
							<li><a href="" class="prev_page icon_left">Previous</a></li>
							<li><a href="" class="current_page">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><span>...</span></li>
							<li><a href="">26</a></li>
							<li><a href="" class="next_page icon_right">Next</a></li>
						</ul>
					</div>
				</div>
			</div>

 		</div>
		<?php
			include 'templates/footer.php'
		?>
		<div class="popup_block delete_popup">
			<div class="popup_inner">
				<div class="popup_container">
					<div class="description_block">Are you sure you want to delete this comics? </div>
					<div class="btns_block">
						<span class="cancel_btn">Cancel</span>
						<button class="delete_btn">Delete</button>
					</div>
				</div>
			</div>
		</div>
	 	<script src="js/main.js"></script>
 	</body>
</html>

