<div class="loader">
    <img src="css/images/storm.svg" alt="" title=""/>
</div>

<div class="header">
	<div class="header_inner">
		<div class="main_logo">
			<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') == false):?> <a href="index.php"><?php endif?>
				<img src="css/images/main_logo.svg" alt="" title=""/>
			<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') == false):?> </a><?php endif?>
		</div>
		<div class="menu_block">
			<div class="menu_inner">
				<ul class="main_menu">
					<li><a href="index.php" class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'index.php') == true):?>current_page<?php endif?>">Գլխավոր</a></li>
					<li>
						<a href="" class="submenu_btn icon_down <?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'listing.php') == true):?>current_page<?php endif?>">Գրադարան</a>
						<ul class="submenu_list">
							<li><a href="listing.php">Reading Comics</a></li>
							<li><a href="listing.php">Animated Comics</a></li>
						</ul>
					</li>
					<li><a href="constructor.php" class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'constructor.php') == true):?>current_page<?php endif?>">Կոնստրուկտոր</a></li>
					<li><a href="methodology.php" class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'methodology.php') == true):?>current_page<?php endif?>">Մեթոդաբանություն</a></li>
					<li><a href="about.php" class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'about.php') == true):?>current_page<?php endif?>">Մեր մասին</a></li>
					<li><a href="faq.php" class="<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'faq.php') == true):?>current_page<?php endif?>">ՀՏՀ</a></li>
				</ul>
				<ul class="lg_list">
					<li><a href="">Am</a></li>
					<li><a href="" class="current_lg">Eng</a></li>
				</ul>
			</div>
		</div>
		<div class="sign_block">
			<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'profile.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'draft.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'profile_edit.php') == true):?> 
				<button class="profile_btn">
					<img src="images/profile.jpg" alt="" title=""/>
				</button>
				<ul class="profile_menu">
					<li><a href="profile">Profile</a></li>
					<li><button>Log Out</button></li>
				</ul>
			<?php endif?>
			<?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'profile.php') == false && strpos($_SERVER['SCRIPT_FILENAME'], 'draft.php') == false && strpos($_SERVER['SCRIPT_FILENAME'], 'profile_edit.php') == false):?> 
				<a href="login.php" class="sign_btn <?php if( strpos($_SERVER['SCRIPT_FILENAME'], 'login.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'register.php') == true || strpos($_SERVER['SCRIPT_FILENAME'], 'pass_forget.php') == true):?>current_page<?php endif?>">Մուտք</a>
			<?php endif?>

			
		</div>
		
		<button class="menu_btn">
			<span></span>
		</button>
	</div>
</div>




