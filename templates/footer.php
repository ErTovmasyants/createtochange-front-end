<div class="footer">
    <div class="footer_top">
        <div class="page_container">
            <div class="partners_section">
                <div class="footer_subtitle">Our Partners</div>
                <ul class="partners_list">
                    <li><img src="css/images/eu_for_armenia.svg" alt="" title=""/></li>
                    <li><img src="css/images/zinanshan.svg" alt="" title=""/></li>
                    <li><img src="css/images/kolba.svg" alt="" title=""/></li>
                    <li><img src="css/images/undp.svg" alt="" title=""/></li>
                </ul>
                <div class="info_block">This website and content were created with the financial support of the European Union. Its contents are the sole responsibility of KASA Swiss Humanitarian Foundation and do not necessarily reflect the views of the European Union. </div>
            </div>
            <div class="kasa_info">
                <div class="image_block">
                    <img src="css/images/kasa.svg" alt="" title=""/>
                </div>
                <div class="info_block">KASA Foundation is currently implementing projects focusing on education, e-Learning, formation of civil society & sustainable development of Armenia.</div>
            </div>
        </div>
    </div>
    <div class="footer_middle">
        <span class="waves">
            <span class="yellow_wave" style="background-image: url('css/images/wave_yellow.svg')"></span>
            <span class="pink_wave" style="background-image: url('css/images/wave_pink.svg')"></span>
            <span class="blue_wave" style="background-image: url('css/images/wave_blue.svg')"></span>
            <span class="white_wave" style="background-image: url('css/images/wave_white.svg')"></span>
        </span>
        <div class="middle_inner">
            <div class="page_container">
                <div class="logo_block">
                    <img src="css/images/main_logo.svg" alt="" title=""/>
                </div>
                <ul class="footer_menu">
                    <li><a href="index.php">Գլխավոր</a></li>
					<li><a href="listing.php">Reading Comics</a></li>
					<li><a href="listing.php">Animated Comics</a></li>
					<li><a href="constructor.php">Կոնստրուկտոր</a></li>
					<li><a href="methodology.php">Մեթոդաբանություն</a></li>
					<li><a href="about.php">Մեր մասին</a></li>
					<li><a href="faq.php">ՀՏՀ</a></li>
                </ul>
                <div class="footer_contacts">
                    <div class="contacts_subtitle">"Espaces" Youth Training Center</div>
                    <ul>
                        <li><a href="tel:+37410541844" class="phone_link icon_phone">+374 10-54-18-44 </a></li>
                        <li><a href="mailto:espaces@kasa.am" class="icon_globe">espaces@kasa.am</a></li>
                        <li><span class="icon_location">Armenia, Yerevan 0001<br/>Nalbandyan 29</span></li>
                        <li><a href="" class="icon_facebook" target="_blank">Create to Change</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="footer_bottom">
        <div class="page_container">
            <div class="page_row">
                <div class="copyrights">© 2021 COPY RIGHT RESERVED, KASSA SWIS <a href="terms.php" target="_blank">Copyright &#38; Terms of Use</a></div>
                <div class="developer">Development by <a href="" target="_blank">Smart Apaga</a></div>
            </div>
        </div>
    </div>
	
</div>

