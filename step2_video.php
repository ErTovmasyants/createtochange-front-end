  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/comicsCreate.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/attach.js"></script>
		<script src="js/select.js"></script>
		<script src="js/jquery.form-validator.js"></script>
		<script src="js/autosize.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="profile.php">My Profile</a></li>
						<li><div>Comics create</div></li>
					</ul>
				</div>
			</div>
			<div class="create_page">
				<div class="large_container">
					<div class="steps_block">
						<ul class="steps_list">
							<li class="current_step">Step <span class="step_num">2</span></li>
							<li>Step <span class="step_num">1</span></li>
						</ul>
					</div>
					<h1 class="page_title">Step 2</h1>
					<form class="create_section">
						<div class="comics_tree">
							<span class="toggle_btn"></span>
							<div class="tree_inner">
								<div class="tree_list">
									<div class="version_block">
										<span class="sublist_toggle"></span>
										<div class="version_name">Starting comics</div>
										<div class="action_btns">
											<a href="" class="add_version icon_plus"></a>
											<a href="" class="edit_version icon_pen"></a>
											<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
										</div>
									</div>
									<ul class="branches_list">
										<li>
											<div class="version_block">
												<span class="sublist_toggle"></span>
												<div class="version_name">Տարբերակ 1</div>
												<div class="action_btns">
													<a href="" class="add_version icon_plus"></a>
													<a href="" class="edit_version icon_pen"></a>
													<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
												</div>
											</div>
											<ul>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 1</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block current">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
															<ul>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 1</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 2</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
															</ul>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
															<ul>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 1</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 2</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 3</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
															</ul>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 3</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
															<ul>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 1</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 2</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 3</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
																<li>
																	<div class="version_block">
																		<span class="sublist_toggle"></span>
																		<div class="version_name">Տարբերակ 4</div>
																		<div class="action_btns">
																			<a href="" class="add_version icon_plus"></a>
																			<a href="" class="edit_version icon_pen"></a>
																			<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																		</div>
																	</div>
																</li>
															</ul>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 4</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 2</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 3</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 3</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
											</ul>
										</li>
										<li>
											<div class="version_block">
												<span class="sublist_toggle"></span>
												<div class="version_name">Տարբերակ 2</div>
												<div class="action_btns">
													<a href="" class="add_version icon_plus"></a>
													<a href="" class="edit_version icon_pen"></a>
													<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
												</div>
											</div>
											<ul>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 1</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 3</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 4</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 2</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 3</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 4</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 3</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 3</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 4</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 4</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
													<ul>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 1</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 2</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 3</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
														<li>
															<div class="version_block">
																<span class="sublist_toggle"></span>
																<div class="version_name">Տարբերակ 4</div>
																<div class="action_btns">
																	<a href="" class="add_version icon_plus"></a>
																	<a href="" class="edit_version icon_pen"></a>
																	<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
																</div>
															</div>
														</li>
													</ul>
												</li>
											</ul>
										</li>
										<li>
											<div class="version_block">
												<span class="sublist_toggle"></span>
												<div class="version_name">Տարբերակ 3</div>
												<div class="action_btns">
													<a href="" class="add_version icon_plus"></a>
													<a href="" class="edit_version icon_pen"></a>
													<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
												</div>
											</div>
											<ul>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 1</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 2</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
												</li>
											</ul>
										</li>
										<li>
											<div class="version_block">
												<span class="sublist_toggle"></span>
												<div class="version_name">Տարբերակ 4</div>
												<div class="action_btns">
													<a href="" class="add_version icon_plus"></a>
													<a href="" class="edit_version icon_pen"></a>
													<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
												</div>
											</div>
											<ul>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 1</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 2</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
												</li>
												<li>
													<div class="version_block">
														<span class="sublist_toggle"></span>
														<div class="version_name">Տարբերակ 3</div>
														<div class="action_btns">
															<a href="" class="add_version icon_plus"></a>
															<a href="" class="edit_version icon_pen"></a>
															<span class="popup_btn icon_delete" data-popup="delete_popup"></span>
														</div>
													</div>
												</li>
											</ul>
										</li>
									</ul>
								</div>
							</div>
						</div>
						<div class="upload_section">
							<div class="section_inner">
								<div class="version_title">
									<div class="field_block">
										<div class="field_name">Version title</div>
										<input type="text" name="version_title" placeholder="Version title" data-validation="required"/>		<span class="error_hint">This section is requited </span>
									</div>
								</div>
								<div class="files_list">
									<div class="attach_block">
										<label class="attach_label">
											<input type="file" data-type="video" name="comics_file[]" data-preview="on" data-maxsize="100" accept=".mp4" data-sizeerror="large file(max. 100mb)" data-typeerror="invalid file format (.mp4 only)"/>
											<span class="attach_btn icon_video"></span>
										</label>
									</div>
								</div>
								<div class="save_btn_block">
									<a href="" class="save_btn">Save</a>
								</div>
							</div>
						</div>
						<div class="btns_section">
							<a href="" class="draft_btn">Draft</a>
							<button class="next_step_btn">Publish</button>
						</div>
					</form>
				</div>
			</div>
 		</div>
		 
		<?php
			include 'templates/footer.php'
		?>
		<div class="popup_block delete_popup">
			<div class="popup_inner">
				<div class="popup_container">
					<div class="description_block">are you sure you want to remove this version and all its subversions?</div>
					<div class="btns_block">
						<span class="cancel_btn">Cancel</span>
						<button class="delete_btn">Delete</button>
					</div>
				</div>
			</div>
		</div>
		<div class="tools_block">
			<div class="attach_block">
				<label class="attach_label">
					<input type="file" data-type="video" name="comics_file[]" data-preview="on" data-maxsize="100" accept=".mp4" data-sizeerror="large file(max. 100mb)" data-typeerror="invalid file format (.mp4 only)"/>
					<span class="attach_btn icon_video"></span>
				</label>
			</div>
			<div class="actions_block">
				<span class="action_toggle icon_pen"></span>
				<ul class="actions_list">
					<li>
						<label class="attach_label">
							<span class="attach_btn icon_refresh">Replace</span>
						</label>
					</li>
					<li><span class="icon_delete attach_remove">Delete</span></li>
				</ul>
			</div>
		</div>
	 	<script src="js/main.js"></script>
 	</body>
</html>