  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/faq.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><div>FAQ</div></li>
					</ul>
				</div>
			</div>
			<div class="faq_inner">
				<div class="page_container">
					<h1 class="page_title">FAQ</h1>
					<ul class="faq_list">
						<li>
							<span class="num_block">1</span>
							<div class="question_block">Can I create a new comic without registering?
							</div>
							<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							<button class="toggle_btn icon_down">Answer</button>
						</li>
						<li>
							<span class="num_block">2</span>
							<div class="question_block">How can I register on the website?</div>
							<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							<button class="toggle_btn icon_down">Answer</button>
						</li>
						<li>
							<span class="num_block">3</span>
							<div class="question_block">Is it possible to save the draft version of the comic before having it published?
							</div>
							<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							<button class="toggle_btn icon_down">Answer</button>
						</li>
						<li>
							<span class="num_block">4</span>
							<div class="question_block">Is it possible to save the draft version of the comic before having it published?</div>
							<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							<button class="toggle_btn icon_down">Answer</button>
						</li>
						<li>
							<span class="num_block">5</span>
							<div class="question_block">How can I register on the website?</div>
							<div class="answer_block">You can watch ready comics without registering, but to create a new comic you need to register.</div>
							<button class="toggle_btn icon_down">Answer</button>
						</li>
					</ul>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>