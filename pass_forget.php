  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		  <link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/sign.css">
		
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/jquery.form-validator.js"></script>
		<script src="js/select.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			 <div class="sign_inner">
				<div class="welcome_block">
					<h1 class="large_title">Hi! Welcome back</h1>
					<div class="welcome_text">Create to change is a platform where you can upload your comics or animated videos </div>
					<div class="image_block">
						<img src="images/welcome_image.jpg" alt="" title=""/>
					</div>
				</div>
				<div class="sign_block">
					<div class="title_block">
						<h2 class="page_title">Forgot Password</h2>
					</div>
					<div class="form_container">
						<!-- <div class="login_error">Wrong E-mail or Password, please, try again</div> -->
						<!-- <div class="login_success">Wrong E-mail or Password, please, try again</div> -->
						<form>
							<div class="field_block">
								<div class="field_name">E-mail address</div>
								<input type="text" name="log_email" placeholder="E-mail address" data-validation="email"/>
								<span class="error_hint">
									<span class="standard_hint">This section is requited </span>
									<span class="individual_hint">Invalid E-mail address</span>
								</span>
							</div>
							<div class="btn_block">
								<button class="validate_btn">Send</button>
							</div>
						</form>
					</div>
				</div>
			 </div>
			
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>