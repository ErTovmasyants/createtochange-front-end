  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/index.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="main_section">
				<div class="section_inner">
					<div class="section_block">
						<h2 class="section_title"><a href="listing.php">Reading Comics</a></h2>
						<ul class="products_list">
							<li>
								<a class="product_block" href="product_inner.php">
									<span class="image_block">
										<img src="images/comics_image1.jpg" alt="" title=""/>
									</span>
									<span class="comics_name">The Queen who made the king get a job</span>
								</a>
							</li>
							<li>
								<a class="product_block" href="product_inner.php">
									<span class="image_block">
										<img src="images/comics_image2.jpg" alt="" title=""/>
									</span>
									<span class="comics_name">The Queen who made the king get a job</span>
								</a>
							</li>
							<li>
								<a class="product_block" href="product_inner.php">
									<span class="image_block">
										<img src="images/comics_image3.jpg" alt="" title=""/>
									</span>
									<span class="comics_name">The Queen who made the king get a job</span>
								</a>
							</li>
						</ul>
						<div class="see_more">
							<a href="listing.php" class="icon_arrow">See more</a>
						</div>
					</div>
					<div class="section_block">
						<h2 class="section_title"><a href="listing.php">Animated Comics</a></h2>
						<ul class="products_list">
							<li>
								<a class="product_block" href="product_inner.php">
									<span class="image_block">
										<img src="images/comics_image4.jpg" alt="" title=""/>
									</span>
									<span class="comics_name">The Queen who made the king get a job</span>
								</a>
							</li>
							<li>
								<a class="product_block" href="product_inner.php">
									<span class="image_block">
										<img src="images/comics_image5.jpg" alt="" title=""/>
									</span>
									<span class="comics_name">The Queen who made the king get a job</span>
								</a>
							</li>
							<li>
								<a class="product_block" href="product_inner.php">
									<span class="image_block">
										<img src="images/comics_image6.jpg" alt="" title=""/>
									</span>
									<span class="comics_name">The Queen who made the king get a job</span>
								</a>
							</li>
						</ul>
						<div class="see_more">
							<a href="listing.php" class="icon_arrow">See more</a>
						</div>
					</div>
				</div>
			</div>

			<div class="about_section">
				<div class="page_container">
					<h2 class="section_title">Be creative! Log in and upload your comics here</h2>
					<div class="description_block">The site was created within the framework of the "Create for Change" program, implemented by the "KASA" SwissThe site was created within the framework of the "Create for Change" program, implemented by the "KASA" Swiss  </div>
					<ul class="steps_list">
						<li>
							<div class="step_name">Register</div>
							<a href=""><img src="css/images/step_2.svg" alt="" title=""/></a>
						</li>
						<li>
							<div class="step_name">Upload your comics</div>
							<a href=""><img src="css/images/step_1.svg" alt="" title=""/></a>
						</li>
						<li>
							<div class="step_name">Make interactive</div>
							<a href=""><img src="css/images/step_3.svg" alt="" title=""/></a>
						</li>
						<li>
							<div class="step_name">Share with friends</div>
							<a href=""><img src="css/images/step_4.svg" alt="" title=""/></a>
						</li>
					</ul>
					<div class="about_join">
						<a href="sign.php">Sign up</a>
						<div class="text_block">Join us and make comics</div>
					</div>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>