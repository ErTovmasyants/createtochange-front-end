  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/comicsCreate.css">
		
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/attach.js"></script>
		<script src="js/select.js"></script>
		<script src="js/jquery.form-validator.js"></script>
		<script src="js/autosize.js"></script>
		<script src="js/jquery.fancybox.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="profile.php">My Profile</a></li>
						<li><div>Comics create</div></li>
					</ul>
				</div>
			</div>
			<div class="create_page">
				<div class="page_container">
					
					<div class="steps_block">
						
						<ul class="steps_list">
							<li>Step <span class="step_num">2</span></li>
							<li class="current_step">Step <span class="step_num">1</span></li>
						</ul>
					</div>
					<h1 class="page_title">Step 1</h1>
					<a class="guide_link" href="https://youtu.be/UhVjp48U2Oc" data-fancybox="guide_video">How To Use</a>
					<form class="main_options">
						<div class="page_row">
							<div class="comics_poster attach_block">
								<div class="image_block"></div>
								<div class="actions_block">
									<div class="actions_label">Cover photo</div>
									<label class="attach_label">
										<input type="file" name="comics_poster" data-preview="on" data-maxsize="5" accept=".png, .jpg, .jpg" data-sizeerror="large file(max. 5mb)" data-validation="required" data-typeerror="invalid file format (.png, .jpg or .jpg)" data-validation="required"/>
										<span class="attach_btn icon_edit"></span>
										<span class="error_hint">Poster is required</span>
									</label>
									<span class="attach_remove icon_delete" data-popup="delete_popup"></span> 
								</div>
							</div>
							<div class="fields_section">
								<div class="info_fields">
									<div class="field_block">
										<div class="field_name">Title</div>
										<input type="text" name="comics_title" placeholder="Comics Title" data-validation="required"/>				<span class="error_hint">This section is requited </span>
									</div>
									<div class="field_block">
										<div class="field_name">Description</div>
										<textarea name="comics_description" placeholder="Comics Description" data-validation="required"></textarea>
										<span class="error_hint">This section is requited</span>
									</div>
									<div class="field_block">
										<div class="field_name">Co-owners</div>
										<textarea name="comics_co_owner" placeholder="Comics Co-owners" data-validation="required"></textarea>
										<span class="error_hint">This section is requited</span>
									</div>
									<div class="field_block">
										<div class="field_name">Pdf</div>
										<div class="attach_block">
											<label class="attach_label">
												<input type="file" accept=".pdf" data-type="simple" data-maxsize="50" data-sizeerror="large file(max. 50mb)" data-typeerror="invalid file format (only pdf)"/>
												<span class="attach_btn">Upload comics pdf here</span>
											</label>
										</div>
										
									</div>
								</div>
								<div class="select_fields">
									<div class="field_block">
										<div class="field_name">Language</div>
										<select name="comics_language"  data-placeholder="Select Language">
											<option value="1">Armenian</option>
											<option value="2">English</option>
										</select>
									</div>
									<div class="field_block">
										<div class="field_name">Type</div>
										<select name="comics_type" data-placeholder="Select Type">
											<option value="1">Animated</option>
											<option value="2">Reading</option>
										</select>
									</div>
									<div class="field_block">
										<div class="field_name">Category</div>
										<select name="comics_category" data-placeholder="Select Category">
											<option value="Youth_Participation">Youth Participation</option>
											<option value="Human_Rights">Human Rights</option>
											<option value="Media_Literacy">Media Literacy</option>
											<option value="Culture">Culture</option>
											<option value="Education">Education</option>
											<option value="Enviromental">Enviromental</option>
											<option value="Others">Others</option>
										</select>
									</div>
								</div>
								<div class="btns_section">
									<a href="" class="draft_btn">Save Draft</a>
									<button class="next_step_btn validate_btn">Next Step</button>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
 		</div>
		 
		<?php
			include 'templates/footer.php'
		?>
		<div class="popup_block delete_popup">
			<div class="popup_inner">
				<div class="popup_container">
					<div class="description_block">Are you sure you want to delete comics poster?</div>
					<div class="btns_block">
						<span class="cancel_btn">Cancel</span>
						<button class="delete_btn">Delete</button>
					</div>
				</div>
			</div>
		</div>
	 	<script src="js/main.js"></script>
 	</body>
</html>