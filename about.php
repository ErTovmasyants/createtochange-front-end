  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/jquery.scrollbar.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/about.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/jquery.scrollbar.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="profile.php">My Profile</a></li>
						<li><div>Comics create</div></li>
					</ul>
				</div>
			</div>
			<div class="about_bnner">
				<img src="images/about_bnner.jpg" alt="" title=""/>
			</div>
			<div class="text_section">
				<div class="page_container">
					<h1 class="page_title">About us</h1>
					<div class="description_block">The site was created within the framework of the "Create for Change" program, implemented by the "KASA" Swiss Humanitarian Foundation with the financial support of the European Union and Kolba Lab’s “Future Today Empowerment of Women, Youth and Children for the Strengthening of Democracy in Armenia”programme.
						<br/><br/>
						22 creative young people are involved in the project who have worked for 6 months in Yerevan’s and Gyumri’s creative teams  and have developed comics and animations on youth participation, media literacy, labor rights, "Gyumri-speaking monuments and buildings" and other topics that you can read and watch on the website. The website is also an open platform to make your comics and animations in an interactive way, as well as to publish them and make them accessible to a wide audience.
						<br/>
						KASA" supports the Armenians pursuing the sustainable development of their country, who are involved in the economic, social and cultural life of Armenia in accordance with the principles of democracy. The foundation operates in 5 spheres: humanitarian, educational, tourism, agricultural, construction.
						<br/>Our team consists of about 50 employees, operating in 3 different locations: the administrative office, the "Espases" Center in Yerevan, the "KASA Gyumri" Center in Gyumri. We also offer guest houses in the centers of Yerevan, Gyumri, we are ready to organize all your tours in Armenia, individually or in groups.
						<br/><br/>
						KASA Foundation: <a href="http://kasa.am/en/" target="_blank">http://kasa.am/en/</a>
						<br/>Kolba Lab: <a href="http://kolba.am/" target="_blank">http://kolba.am/</a>"
					</div>
				</div>
			</div>
			<div class="participants">
				<h2 class="page_title">Our Participants</h2>
				<div class="description_block">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of </div>
				<div class="members_list">
					<ul>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Simona</div>
									<div class="about_member">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile2.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Cupcake</div>
									<div class="about_member">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. </div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile3.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Twinkle toes baby</div>
									<div class="about_member">I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile4.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Twinkle toes</div>
									<div class="about_member"> I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile5.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Animegirl</div>
									<div class="about_member">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Simona</div>
									<div class="about_member">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile2.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Cupcake</div>
									<div class="about_member">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. </div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile3.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Twinkle toes baby</div>
									<div class="about_member">I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile4.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Twinkle toes</div>
									<div class="about_member"> I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
						<li>
							<div class="member_block">
								<div class="member_image">
									<img src="images/profile5.jpg" alt="" title=""/>
								</div>
								<div class="info_block">
									<div class="member_name">Animegirl</div>
									<div class="about_member">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
									<a href="" class="member_link icon_arrow">Profile</a>
								</div>
							</div>
						</li>
					</ul>
				</div>
			</div>
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>