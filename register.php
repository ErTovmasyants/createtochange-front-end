  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		  <link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/sign.css">
		
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/jquery.form-validator.js"></script>
		<script src="js/select.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			 <div class="sign_inner">
				<div class="welcome_block">
					<h1 class="large_title">Welcome</h1>
					<div class="welcome_text">Create to change is a platform where you can upload your comics or animated videos </div>
					<div class="image_block">
						<img src="images/welcome_image.jpg" alt="" title=""/>
					</div>
				</div>
				<div class="sign_block">
					<ul class="switch_buttons">
						<li><a href="login.php">Log In</a></li>
						<li><a href="register.php" class="selected">Sign Up</a></li>
					</ul>
					<div class="form_container">
						<!-- <div class="login_error">Wrong E-mail or Password, please, try again</div> -->
						<!-- <div class="login_success">Wrong E-mail or Password, please, try again</div> -->
						<form>
							<div class="form_fields">
								<div class="field_block">
									<div class="field_name">Name</div>
									<input type="text" name="full_name" placeholder=" Name" data-validation="required"/>									<span class="error_hint">This section is requited </span>
								</div>
								<div class="field_block">
									<div class="field_name">Surname</div>
									<input type="text" name="full_name" placeholder="Surname" data-validation="required"/>
									<span class="error_hint">This section is requited </span>
								</div>
								<div class="field_block">
									<div class="field_name">Nickname</div>
									<input type="text" name="username" placeholder="Nickname" data-validation="required"/>
									<span class="error_hint">This section is requited </span>
								</div>
								<div class="field_block">
									<div class="field_name">Phone Number</div>
									<input type="text" name="phone_number" placeholder="Phone Number" data-validation="required" oninput="this.value=this.value.replace(/[^0-9+]/g,'');"/>
									<span class="error_hint">This section is requited</span>
								</div>
								<div class="field_block full_field">
									<div class="field_name">E-mail address</div>
									<input type="text" name="email" placeholder="E-mail address" data-validation="email"/>
									<span class="error_hint">
										<span class="standard_hint">This section is requited </span>
										<span class="individual_hint">Invalid E-mail address</span>
									</span>
								</div>
								<div class="field_block">
									<div class="field_name">Country</div>
									<select name="country" data-placeholder="Country">
										<option></option>
										<option value="1">Armenia</option>
										<option value="2">Georgia</option>
										<option value="3">Russia</option>
										<option value="4">Belarus</option>
										<option value="5">Kazakhstan</option>
									</select>
								</div>
								<div class="field_block">
									<div class="field_name">City</div>
									<select name="country" data-placeholder="City" disabled>
										<option></option>
										<option value="1">Yerevan</option>
										<option value="2">Abovyan</option>
										<option value="3">Goris</option>
										<option value="4">Kapan</option>
										<option value="5">Meghri</option>
									</select>
								</div>
								<div class="field_block">
									<div class="field_name">Password</div>
									<input class="password_field" type="password" name="reg_pass" data-validation="required" placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"/>
									<span class="error_hint">This section is requited</span>
									<span class="type_switch"></span>
								</div>
								<div class="field_block">
									<div class="field_name">Confirm password</div>
									<input class="confirm_field" type="password" name="pass_confirm" placeholder="&#8226; &#8226; &#8226; &#8226; &#8226; &#8226; &#8226;"/>
									<span class="error_hint">Passwords do not match</span>
									<span class="type_switch"></span>
								</div>
							</div>
							<div class="btn_block">
								<button class="validate_btn">Sign Up</button>
							</div>
						</form>
					</div>
				</div>
			 </div>
			
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>