  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		  <link rel="stylesheet" href="css/select.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/sign.css">
		
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/jquery.form-validator.js"></script>
		<script src="js/select.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			 <div class="sign_inner">
				<div class="welcome_block">
					<div class="image_block">
						<img src="images/welcome_image.jpg" alt="" title=""/>
					</div>
				</div>
				<div class="sign_block">
					<div class="title_block">
						<h2 class="page_title"><span class="num404">404</span> Page not found</h2>
					</div>
					<div class="success_info">
						<!-- <div class="login_error">Wrong E-mail or Password, please, try again</div> -->
						<div class="login_success">Something is wrong...</div>
						<a href="" class="homepage_btn">Go to homepage</a>
					</div>
				</div>
			 </div>
			
 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>