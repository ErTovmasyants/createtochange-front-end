  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/listing.css">
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><div>Animated Comics</div></li>
					</ul>
				</div>
			</div>
			<div class="listing_inner">
				<div class="page_container">
					<h1 class="page_title">Animated Comics</h1>
					<div class="category_filter" data-more="More">
						<ul>
							<li><a href="" class="selected" data-text="All Categories"><span>All Categories</span></a></li>
							<li><a href="" data-text="Youth Participation"><span>Youth Participation</span></a></li>
							<li><a href="" data-text="Human Rights"><span>Human Rights</span></a></li>
							<li><a href="" data-text="Media Literacy"><span>Media Literacy</span></a></li>
							<li><a href="" data-text="Culture"><span>Culture</span></a></li>
							<li><a href="" data-text="Education"><span>Education</span></a></li>
							<li><a href="" data-text="Enviromental"><span>Enviromental</span></a></li>
							<li><a href="" data-text="Others"><span>Others</span></a></li>
						</ul>
					</div>
					<ul class="products_list">
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image1.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image2.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image3.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image4.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image5.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image6.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image5.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image6.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image2.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image4.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image3.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
						<li>
							<a class="product_block" href="product_inner.php">
								<span class="image_block">
									<img src="images/comics_image1.jpg" alt="" title=""/>
								</span>
								<span class="comics_name">The Queen who made the king get a job</span>
							</a>
							<div class="comics_lg">
								<span class="lg_label">Language:</span> English
							</div>
						</li>
					</ul>
					<div class="paging">
						<ul>
							<li><a href="" class="prev_page icon_left">Previous</a></li>
							<li><a href="" class="current_page">1</a></li>
							<li><a href="">2</a></li>
							<li><a href="">3</a></li>
							<li><span>...</span></li>
							<li><a href="">26</a></li>
							<li><a href="" class="next_page icon_right">Next</a></li>
						</ul>
					</div>
				</div>
			</div>

 		</div>
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
 	</body>
</html>