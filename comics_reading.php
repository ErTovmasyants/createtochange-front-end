  <!DOCTYPE HTML>
<html>
 	<head>
  		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0"/>
		<meta name="format-detection" content="telephone=no"/>
  		<title>CREATE to CHANGE</title>
		<link rel="stylesheet" href="css/jquery.fancybox.css">
		<link rel="stylesheet" href="css/main.css">
		<link rel="stylesheet" href="css/comics.css">
		
		<?php
			include 'templates/favicons.php'
		?>
		<script src="js/jquery-3.3.1.js"></script>
		<script src="js/jquery.fancybox.js"></script>
 	</head>
 	<body>
 		<?php
			include 'templates/header.php'
		?>
 		<div class="content">
			<div class="breadcrumbs">
				<div class="page_container">
					<a href="" class="back_btn icon_arrow">Back</a>
					<ul>
						<li><a href="index.php">Home</a></li>
						<li><a href="profile.php">My Profile</a></li>
						<li><div>Comics create</div></li>
					</ul>
				</div>
			</div>
			<div class="comics_page">
				<div class="comics_main">
					<div class="page_container">
						<div class="container_inner">
							<h1 class="page_title">The Queen who made the king get a job</h1>
							<div class="image_block">
								<img src="images/comics_image1.jpg" alt="" title=""/>
							</div>
							<div class="info_block">
								<div class="short_info">
									<div class="description_block">I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life I am  a comics artist. I am a bit sad in this picture but I am quite sure that in the final version of the website my picture will be with a smiley happy face: just the way I am in real life :)</div>
								</div>
								<ul class="comics_params">
									<li>Language: <span class="param_info">English</span></li>
									<li>Category: <span class="param_info">Human Rights</span></li>
								</ul>
								<div class="comics_creators">
									Story by- Ani Avagyan, Hovhannes Hovhannisyan, Diana Khachatryan
									<br/>Illustration- Serine Zohrabyan, Aida Sargsyan
									<br/>Coloring- Samvel Simonyan
									<br/>Animation- Shahane Ghahramanyan
									<br/>Mounting- Tigran Asatryan
									<br/>Music- Siranush Hakobyan
								</div>
							</div>
							<div class="share_btns">
								<div class="share_label">Share</div>
								<div class="addthis_inline_share_toolbox_c8yg"></div>
							</div>
						</div>
						
					</div>
				</div>
				<div class="comics_inner" data-comics="comics1">
					<div class="comics_block">
						<div class="page_container">
							<a href="images/comics_img1.jpg" data-fancybox="comics_images"><img src="images/comics_img1.jpg" alt="" title=""/></a>
							<a href="images/comics_img2.jpg" data-fancybox="comics_images"><img src="images/comics_img2.jpg" alt="" title=""/></a>
							<a href="images/comics_img3.jpg" data-fancybox="comics_images"><img src="images/comics_img3.jpg" alt="" title=""/></a>
						</div>
					</div>
					<div class="download_btn">
						<a href="">Download PDF</a>
					 </div>
				</div>
			</div>
			
 		</div>
		 <button class="back_to_top icon_down"></button>
		 
		<?php
			include 'templates/footer.php'
		?>
	 	<script src="js/main.js"></script>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-589071e66b72346f"></script>
 	</body>
</html>